create table customer (
  id varchar(36),
  email varchar(100) not null,
  fullname varchar(255) not null,
  primary key (id),
  unique (email)
);

create table sales (
  id varchar(36),
  id_customer varchar(36) not null,
  sales_number varchar(100) not null,
  transaction_time timestamp not null,
  primary key (id),
  foreign key (id_customer) references customer
);

create table sales_detail (
  id varchar(36),
  id_sales varchar(36) not null,
  product_code varchar(100) not null,
  quantity integer not null,
  unit_price decimal(19,2) not null,
  primary key(id),
  foreign key (id_sales) references sales
);